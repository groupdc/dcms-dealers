@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>Dealers</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li><a href="{!! URL::to('admin/dealers') !!}"><i class="far fa-map-marker-alt"></i> Dealers</a></li>
            @if(isset($dealer))
                <li class="active"><i class="far fa-pencil"></i> Dealer {{$dealer->id}}</li>
            @else
                <li class="active"><i class="far fa-plus-circle"></i> Create dealer</li>
            @endif
        </ol>
        @if(isset($dealer))
            <p class="edit">Last edited by {{ $dealer->admin }} on {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $dealer->updated_at)->format('d-m-Y H:i:s') }}</p>
        @endif
    </div>

    <div class="main-content">
        <div class="row">
            
            @if(isset($dealer))
                {!! Form::model($dealer, array('route' => array('admin.dealers.update', $dealer->id), 'method' => 'PUT', 'onsubmit' => 'return resetTables();')) !!}
            @else
                {!! Form::open(array('url' => 'admin/dealers','onsubmit' => 'return resetTables();')) !!}
            @endif

            @if(!is_null($extendformTemplate["template"]))
                @include($extendformTemplate["template"], array('dealer'=>(isset($dealer)?$dealer:null)))
            @endif

            <div class="col-md-12">
                <div class="main-content-tab tab-container">

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#data" role="tab" data-toggle="tab">Data</a></li>
                        <li><a href="#labels" role="tab" data-toggle="tab">Labels</a></li>
                        <li><a href="#pages" role="tab" data-toggle="tab">Pages</a></li>
                        <li><a href="#categorylabels" role="tab" data-toggle="tab">Category Labels</a></li>
                        <li><a href="#closures" role="tab" data-toggle="tab">Closure</a></li>
                        <li><a href="#openings" role="tab" data-toggle="tab">Openings</a></li>
                    </ul>

                    <div class="tab-content">

                        @if($errors->any())
                            <div class="alert alert-danger">{!! HTML::ul($errors->all()) !!}</div>
                        @endif

							<div id="data" class="tab-pane active">

                <div class="form-group">
                  {!! Form::label('code', 'Code') !!}
                  {!! Form::text('code', null, array('class' => 'form-control')) !!}
                </div>

                  <div class="form-group">
                    {!! Form::label('code_NL', 'Code NL - exactcode') !!}
                    {!! Form::text('code_NL', null, array('class' => 'form-control')) !!}
                  </div>

                            <div class="form-group">
                                {!! Form::label('dealer', 'Dealer') !!}
                                {!! Form::text('dealer', null, array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('address', 'Address') !!}
                                {!! Form::text('address', null, array('class' => 'form-control')) !!}
                            </div>

                            <div class="row">
                                <div class="col-sm-2">

                                    <div class="form-group">
                                        {!! Form::label('zip', 'Zip') !!}
                                        {!! Form::text('zip', null, array('class' => 'form-control')) !!}
                                    </div>

                                </div>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        {!! Form::label('city', 'City') !!}
                                        {!! Form::text('city', null, array('class' => 'form-control')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('country_id', 'Country') !!}
                                {!! Form::select('country_id', $countries, old('country_id'), array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('longitude', 'longitude') !!}
                                {!! Form::text('longitude', null, array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('latitude', 'Latitude') !!}
                                {!! Form::text('latitude', null, array('class' => 'form-control')) !!}
                            </div>


                            <div class="form-group">
                                {!! Form::label('phone', 'Phone') !!}
                                {!! Form::text('phone', null, array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('email', 'Email') !!} Shown on the dealer page on the websites
                                {!! Form::text('email', null, array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('contactemail', 'Contact email') !!} used for B2C Naturapy&reg; orders. Accepts multiple with ";" separator
                                {!! Form::text('contactemail', null, array('class' => 'form-control')) !!}
                            </div>
			    

              <div class="form-group">
                <label for="contactlanguage">Contact language</label>
                <select name="contactlanguage" id="contactlanguage" class="form-control">
                  <option value="">-</option>
                  <option value="nl" @if(isset($dealer) && $dealer->contactlanguage == 'nl') selected @endif  >nl</option>
                  <option value="fr" @if(isset($dealer) && $dealer->contactlanguage == 'fr') selected @endif>fr</option>
                </select>
              </div>
	      
	       <div class="form-group">
                  {!! Form::label('attention', 'Attention') !!} <span class="">extra info used for DPD shipment label</span>
                  {!! Form::text('attention', null, array('class' => 'form-control')) !!}
               </div>

                            <div class="form-group">
                                {!! Form::label('website', 'Website') !!}
                                {!! Form::text('website', "http://".str_replace(["http://", "https://"],"",(isset($dealer->website)?$dealer->website:old('website'))), array('class' => 'form-control')) !!}
                            </div>

                        </div>

                        <!-- Labels -->
                        <div id="labels" class="tab-pane">

                            <div class="form-group">
                                {!! Form::checkbox('naturapy', 1, null, array('class' => 'form-checkbox' , 'id' => 'naturapy')) !!}
                                {!! HTML::decode(Form::label('naturapy', "Naturapy", array('class' => (isset($dealer) && $dealer->naturapy===1)?'checkbox marginright active':'checkbox marginright'))) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::checkbox('tuincenter', 1, null, array('class' => 'form-checkbox' , 'id' => 'tuincenter')) !!}
                                {!! HTML::decode(Form::label('tuincenter', "Hobby", array('class' => (isset($dealer) && $dealer->tuincenter===1)?'checkbox marginright active':'checkbox marginright'))) !!}
                            
                                {!! Form::checkbox('premium', 1, null, array('class' => 'form-checkbox' , 'id' => 'premium')) !!}
                                {!! HTML::decode(Form::label('premium', "Premium", array('class' => (isset($dealer) && $dealer->premium===1)?'checkbox marginright active':'checkbox marginright'))) !!}
                            
                                {!! Form::checkbox('naturapypickup', 1, null, array('class' => 'form-checkbox' , 'id' => 'naturapypickup')) !!}
                                {!! HTML::decode(Form::label('naturapypickup', "Naturapy Pickuppoint", array('class' => (isset($dealer) && $dealer->naturapypickup===1)?'checkbox marginright active':'checkbox marginright'))) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::checkbox('procenter', 1, null, array('class' => 'form-checkbox' , 'id' => 'procenter')) !!}
                                {!! HTML::decode(Form::label('procenter', "Professionioneel", array('class' => (isset($dealer) && $dealer->procenter===1)?'checkbox marginright active':'checkbox marginright'))) !!}

                                {!! Form::checkbox('propremium', 1, null, array('class' => 'form-checkbox' , 'id' => 'propremium')) !!}
                                {!! HTML::decode(Form::label('propremium', "Pro Premium", array('class' => (isset($dealer) && $dealer->propremium===1)?'checkbox marginright active':'checkbox marginright'))) !!}
                       
                            </div>

                            <div>
<b>When are the dealers visibile on B2C shop? It depenends on the items in the basket, and the settings above on the dealer.</b>
<br/>
<table border=1 style="width:50%">
<thead>
    <tr>
<td><b><i>Basket</i></u></td>
<td><b><i>Labels on the dealer</i></u></td>
</tr>
</thead>
<tbody>
    <tr>
<td>Home delivery</td>
<td>Naturapy = 1 + Naturapy Pickuppoint = 1</td>
</tr>

<tr>
<td>Home delivery + prepaidcode</td>
<td>Naturapy Pickuppoint = 1</td>
</tr>

<tr>
<td>Pickup</td>
<td>Naturapy = 1 + Naturapy Pickuppoint = 1</td>
</tr>

<tr>
<td>Pickup + prepaidcode</td>
<td>Naturapy Pickuppoint = 1</td>
</tr>
</tbody>

</table>
</div>
                        </div>

                        <!-- Pages -->
                        <div id="pages" class="tab-pane">

                            <div class="form-group">
                                <?php
                                // *Very simple* recursive rendering function
                                function renderNode($node, $pageOptionValuesSelected = array())
                                {
                                    echo '<li class="language-' . $node->language_id . ' depth-' . $node->depth . ' countryhide country-' . $node->language->country_id . ' ">';
                                    echo '<span>';
                                    if ($node->depth == 0) {
                                        echo $node->title . '<i class="far fa-plus-square"></i>';
                                    } else {
                                        $checked = false;
                                        if (in_array($node->id, $pageOptionValuesSelected)) {
                                            $checked = true;
                                        } ?>
                                {!! Form::checkbox("page_id[".$node->language_id."][".$node->id."]", $node->id, $checked, array('class' => 'form-checkbox','id'=>'page_id-'.$node->id))  !!}
                                {!! Form::label('page_id-'.$node->id, $node->title, array('class' => ($checked == true?'active':'').' checkbox','id'=>'chkbxpage_id-'.$node->id)) !!}
                                <?php
                                    }
                                    echo '</span>';

                                    if ($node->children()->count() > 0) {
                                        echo "\r\n" . '<ul class="' . ($node->depth == 0 ? 'division' : ($node->depth == 1 ? 'sector' : 'subsector')) . '">' . "\r\n";
                                        foreach ($node->children as $child) {
                                            renderNode($child, $pageOptionValuesSelected);
                                        }
                                        echo '</ul>' . "\r\n" . "\r\n";
                                    }
                                    echo '</li>' . "\r\n";
                                }


                                $roots = Dcms\Pages\Models\Pageslanguage::whereIsRoot()->get();

                                echo '<ul class="country">';
                                foreach ($roots as $root) {
                                    renderNode($root, $pageOptionValuesSelected);
                                }
                                echo '</ul>';

                                ?>
                            </div>
                        </div>

                        
                        <!-- Categorylabels -->
                        <div id="categorylabels" class="tab-pane">
                            <div class="tab-content">

                                <h2>Overview</h2>
                                <table id="datatable_categorylabels" class="table table-hover table-condensed" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>ID</th>
                                        <th>Label</th>
                                        <th>Country</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <!-- Closures -->
                        <div id="closures" class="tab-pane">

                            <div id="closure">
                                <table class="table table-bordered table-striped ">
                                    <thead>
                                    <tr>
                                        <th>Startdate dd/mm/yyyy (first day of closing)</th>
                                        <th>Enddate dd/mm/yyyy (last day of closing)</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    @if(isset($rowClosure))
                                        {!! $rowClosure !!}
                                    @endif
                                    <tfoot>
                                    <tr>
                                        <td colspan="5"><a class="btn btn-default pull-right add-table-row" href=""><i
                                                        class="far fa-plus"></i></a></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>

                        <!-- Openings -->
                        <div id="openings" class="tab-pane">
                            @for($i = 1; $i <= 7; $i++)
                                <div class="row">
                                    <div class="col-xs-6 col-md-2">
                                        <div class="form-group">
                                            <p class="form-control-static" style="margin: 10px 0"><b>{{ jddayofweek($i - 1, 1) }}</b></p>
                                        </div>
                                    </div>

                                    <div class="col-xs-6 col-md-2">
                                        {!! Form::checkbox('open_'.$i , 1, $openings->where('opening_day', $i)->first(), array('class' => 'form-checkbox' , 'id' => 'open_'.$i)) !!}
                                        {!! Form::label('open_'.$i, "&nbsp;", array('class' => ($openings->where('opening_day', $i)->first())?'checkbox active':'checkbox')) !!}
                                    </div>
                                    @for($j = 1; $j <= 4; $j++)
                                        <div class="col-xs-6 col-md-2">
                                            <div class="form-group">
                                                <input type="time" id="opening_hour_{{$j}}[{{$i}}]" name="opening_hour_{{$j}}[{{$i}}]"
                                                       class="form-control"
                                                       value="{{ $errors->any() ? old('opening_hour_' . $j . '.' . $i) : ($openings->where('opening_day', $i)->first() && $openings->where('opening_day', $i)->first()->{'opening_hour_' . $j} ? Carbon\Carbon::createFromFormat('H:i:s', $openings->where('opening_day', $i)->first()->{'opening_hour_' . $j})->format('H:i') : null)}}">
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                            @endfor

                            <div class="row">

                                <div class="col-xs-6 col-md-2">
                                    <div class="form-group">
                                        AM (00:00 - 11:59)<br/>
                                        PM (12:00 - 23:59)<br/><br/>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-md-2">
                                    <div class="form-group">
                                        12:00 AM = midnight (00:00)<br/>
                                        12:00 PM = midday (12:00)
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div><!-- end tab-content -->
                </div><!-- end main-content-tab -->
            </div><!-- end col-md-12 -->

            <div class="col-md-12">
                <div class="main-content-block">
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
                </div>
            </div>

            {!! Form::close() !!}

        </div><!-- end row -->
    </div><!-- end main-content -->

@stop

@section("script")
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

<script type="text/javascript" src="{!! asset('packages/Dcms/Core/js/bootstrap.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('packages/Dcms/Core/js/jquery-ui-autocomplete.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('packages/Dcms/Core/css/jquery-ui-autocomplete.css') !!}">


    <script type="text/javascript">

        categoryTable = null;
        function resetTables(){
            categoryTable.search('').draw();
            return true;
        }


        $(document).ready(function () {

            //Datatable            
            categoryTable = $('#datatable_categorylabels').DataTable({                
                "paging": false,
                "processing": true,
                "ajax": "{{ route('admin.dealers.api.table.categories', (isset($dealer)?($dealer->id ?: 0):0) ) }}",
                "columns": [
                    {data: 'radio', name: 'radio'},
                    {data: 'id', name: 'id'},
                    {data: 'title', name: 'x.title'},
                    {data: 'language', name: 'language'}
                ],
                "createdRow": function( row, data, cells ) {
                    if (~data['radio'].indexOf('checked')) {   
                    $(row).addClass('selected');
                    }
                },
            });

            //Bootstrap Tabs
            $(".tab-container .nav-tabs a").click(function (e) {
                e.preventDefault()
                $(this).tab('show')
            })

            //UI Autocomplete Product Detail
            $(".main-content-block input[id='zip'], .main-content-block input[id='city']").autocomplete({
                source: function (request, response) {
                    var country_id = $("#country_id").val();
                    $.getJSON("{!! route('admin.dealers.api.zipcity') !!}?zipcity=" + request.term + "&country_id=" + country_id, function (data) {
                        response(data);
                    });
                },
                select: function (event, ui) {
                    $(this).val(ui.item.label);
                    $(this).closest(".main-content-block").find("input[id='zip']").val(ui.item.zip);
                    $(this).closest(".main-content-block").find("input[id='city']").val(ui.item.city);
                    return false;
                },
                minLength: 1,
                delay: 200
            });

        });
    </script>

    <script language="javascript" type="application/javascript">

        $(document).ready(function () {

            $(".countryhide").hide();

            function selectedcountry() {
                $(".countryhide").hide();
                $(".country-" + $("#country_id option:selected").val()).show();

                $(".depth-3").hide();
                $(".depth-1 input:checkbox").not(".depth-2 input:checkbox").remove();
                $(".depth-1 span label").not(".depth-2 span label").attr('class', '');

                $(".country-" + $("#country_id option:selected").val() + " .division").attr('class', 'division active');
            }

            selectedcountry();

            $("#country_id").change(function () {
                selectedcountry();
            });

            //pagetree
            $(".country span").click(function () {
                $(this).find('i').toggleClass('fa-minus-square');
                $(this).next().toggleClass('active');
            });


            //Add table row
            $.fn.addtablerow = function (options) {

                $(this).each(function () {

                    var table = $(this);

                    var rows = table.closest('tbody tr').length;

                    table.find('.add-table-row').click(function () {
                        //  language_id = table.attr("class").replace('table table-bordered table-striped ','');
                        geturl = options.source;
                        //  geturl = geturl.replace("{LANGUAGE_ID}",language_id);

                        $.get(geturl, function (data) {
                            if (!table.find('tbody').length) table.find('thead').after("<tbody></tbody>");

                            data = data.replace(/{INDEX}/g, "extra" + "-" + rows);
                            table.find('tbody').append(data);
                            //$("#attachment-language-id[extra"+rows+"] option[value='"+language_id+"']").attr('selected','selected');
                            rows++;
                            deltablerow(table.find('.delete-table-row').last());
                        });
                        return false;
                    });

                    deltablerow(table.find('.delete-table-row'));

                    function deltablerow(e) {
                        e.click(function () {
                            $(this).closest("tr").remove();
                            if (!table.find('tbody tr').length) table.find('tbody').remove();
                            return false;
                        });
                    }

                });

            };


            $("#closure table").addtablerow({
                source: "{!! URL::to('admin/dealers/api/closurerow?data=closure') !!}" //generate the row with the dropdown fields/empty boxes/etc.
            });
        });
    </script>

@stop
