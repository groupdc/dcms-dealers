@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>Dealers</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li class="active"><i class="far fa-map-marker-alt"></i> Dealers</li>
        </ol>
    </div>

    <div class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">

                    @if (Session::has('message'))
                        <div class="alert alert-info">{!! Session::get('message') !!}</div>
                    @endif

                    @can('dealers-add')
                        <div class="btnbar btnbar-right"><a class="btn btn-small btn-primary" href="{!! URL::to('/admin/dealers/create') !!}">Create new</a></div>
                    @endcan

                    <h2>Overview</h2>
                    <div class="panel filter up">
                        <div class="panel-heading">
                            <h3>Filter <i class="far fa-plus-square right"></i></h3>
                        </div>
                        <div class="panel-body" @if(Session::has('dealerfilter')) style="display:block;" @endif>
                            {!! Form::open(array('url' => 'admin/dealers', 'method' => 'GET')) !!}
                            <div class="form-group" id="filters">
                                <?php
                                if (request()->has('resetfilters') && request()->get('resetfilters') == 'true') {
                                    Session::forget('dealerfilter');
                                }
                                $condition = false;
                                ?>
                                <?php //Session::pull('dealerfilter');?>
                                @if(request()->has('colname') || Session::has('dealerfilter'))
                                @if(request()->has('colname'))
                                <?php
                                Session::put('dealerfilter', request()->all()); //put in a session so the Datatable request can accesss the variables
                                $condition = false;
                                ?>
                                @endif

                                @foreach(Session::get('dealerfilter.colname') as $i => $set_colname)
                                    @if($set_colname != '0')
                                        <?php $condition = true; ?>
                                        <div id="{{$i}}" class="condition">
                                            {!! Form::label('id', 'AND', array('class' => 'form-control left')) !!}
                                            {!! Form::select('colname[]', (array('Filter Column') + $filtercolumns) /*(array('Filter Column') + array_combine(array_values($filtercolumns),$filtercolumns))*/, $set_colname, array('class' => 'form-control left')) !!}
                                            {!! Form::label('id', '=', array('class' => 'form-control left')) !!}
                                            {!! Form::text('colvalue[]', Session::get('dealerfilter.colvalue.'.$i)/*request()->get('colvalue.'.$i)*/, array('class' => 'form-control left')) !!}
                                        </div>
                                    @endif
                                @endforeach
                                @endif
                                @if( (!Session::has('dealerfilter')  && !request()->has('colname')) || $condition == false)
                                    <div id="0" class="condition">
                                        {!! Form::label('id', 'AND', array('class' => 'form-control left')) !!}
                                        {!! Form::select('colname[]', (array('Filter Column') + $filtercolumns) /*(array('Filter Column') + array_combine(array_values($columns),$columns))*/, null, array('class' => 'form-control left')) !!}
                                        {!! Form::label('id', '=', array('class' => 'form-control left')) !!}
                                        {!! Form::text('colvalue[]', '', array('class' => 'form-control left')) !!}
                                    </div>
                                @endif


                                <a class="btn btn-default add-condition" href=""><i class="far fa-plus"></i></a>
                            </div>
                            <div class="btnbar btnbar-left">
                                {!! Form::button('Set Filter', array('class' => 'btn btn-primary', 'type'=>'submit')) !!}
                                <a class="btn btn-default" href="{{url('admin/dealers?resetfilters=true')}}">Reset Filter</a>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>


                    <table id="datatable" class="table table-hover table-condensed" style="width:100%">
                        <thead>
                        <tr>
                              <th>Code</th>
                              <th>Dealer</th>
                            <th>City</th>
                            <th>Country</th>
                            <th>Longitude</th>
                            <th>Latitude</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>

                    <script type="text/javascript">
                        $(document).ready(function () {
                            oTable = $('#datatable').DataTable({
                                "pageLength": 50,
                                "processing": true,
                                "serverSide": true,
                                "ajax": "{{ route('admin.dealers.api.table') }}",
                                "columns": [
                          {data: 'code', name: 'code'},
                          {data: 'dealer', name: 'dealer'},
                                    {data: 'city', name: 'city'},
                                    {data: 'country', name: 'countries.country', searchable: false},
                                    {data: 'longitude', name: 'countries.longitude', searchable: false},
                                    {data: 'latitude', name: 'countries.latitdue', searchable: false},
                                    {data: 'edit', name: 'edit', orderable: false, searchable: false}
                                ]
                            });
                        });
                    </script>

                    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

                    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
                    <script type="text/javascript" language="javascript"
                            src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

                </div>
            </div>
        </div>
    </div>

@stop

@section("script")
    <script type="text/javascript">
        $(document).ready(function () {
            //Filter toggle
            $('.filter .panel-heading').click(function () {
                $(this).closest('.panel').toggleClass('up');
            });

            $("#btn-reset").click(function () {
                var appendDiv = jQuery($(".condition:last")[0].outerHTML);
                $(".condition").remove();
                $("#filters").prepend(appendDiv.attr('id', '0'));
                $("#0").find('select').val('0');
                $("#0").find('input[type=text]').val('');
            });

            //Add condition
            $('.add-condition').click(function () {
                var id = $(".condition:last").attr('id');
                var appendDiv = jQuery($(".condition:last")[0].outerHTML);

                appendDiv.attr('id', ++id).insertAfter(".condition:last");

                $("#" + id).find('select').val('0');
                $("#" + id).find('input[type=text]').val('');

                return false;
            });

        });
    </script>
@stop
