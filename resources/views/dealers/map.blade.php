@extends("dcms::template/layout")

@section("content")
    <div class="main-header">
        <h1>Dealers</h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('admin/dealers') }}"><i class="far fa-map-marker-alt"></i> Dealers</a></li>
            <li class="active"><i class="far fa-map"></i> Map</li>
        </ol>
    </div>

    <div class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">
                    <h2>Filter</h2>
                    {!! Form::open(array('url' => 'admin/dealers/map')) !!}

                    <div class="form-group">
                        {!! Form::label('search_id', 'Search (Dealer, Address, City)') !!}
                        {!! Form::text('search', request()->get('search'), array('class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('country_id', 'Country') !!}
                        {!! Form::select('country_id', array('0'=>'-','1' => 'Belgium', '2' => 'Netherlands', '3' => 'France', '4' => 'Germany'),request()->get('country_id'), array('class' => 'form-control')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Set filter', array('class' => 'btn btn-primary')) !!}
                        <a href="{!! URL::previous() !!}" class="btn btn-default">Reset filter</a>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="main-content-block no-padding">
                    <div id="dealermap"></div>
                </div>
            </div>
        </div>
    </div>
@stop

@section("script")
    <?php
    /*
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', '1');
    */

    $minZoom = 12; //'a better name would be maxZoom - 12 has been defined as highest zoom, (20 is maximum, but not relevant) 12-08-2010


    //$latitude = $minLatitude + ($difLatitude/2);
    //$longitude = $minLongitude + ($difLongitude/2);
    ?>

    <script>

        function initMap() {

            var initCenter = new google.maps.LatLng(50.56318, 4.69528);
            var initZoom = 7;

            var map = new google.maps.Map(document.getElementById('dealermap'), {
                zoom: initZoom,
                center: initCenter
            });
            var bounds = new google.maps.LatLngBounds();

            var dealers = [
                {!!$mapMarkers!!}
            ];

            var iconBase = '/packages/Dcms/dealers/images/marker/';
            var icons = {
                'default': {url: iconBase + 'default.svg', scaledSize: new google.maps.Size(28, 34)},
                'extended': {url: iconBase + 'extended.svg', scaledSize: new google.maps.Size(28, 34)},
                'premium': {url: iconBase + 'premium.svg', scaledSize: new google.maps.Size(28, 34)},
            };

            var infowindow = new google.maps.InfoWindow();

            dealers.forEach(function (dealer) {

                var marker = new google.maps.Marker({
                    position: dealer.position,
                    icon: icons[dealer.type],
                    map: map,
                    zIndex: dealer.zindex,
                    optimized: false,
                });

                bounds.extend(dealer.position);
                map.fitBounds(bounds);

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.close();
                    infowindow.setContent(dealer.info);
                    infowindow.open(map, marker);
                });
            });

            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
                google.maps.event.removeListener(boundsListener);
            });
        }
    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ env("BROWSER_GOOGLE_MAPS_KEY") }}&callback=initMap"></script>

@stop
