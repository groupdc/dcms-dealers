<?php

namespace    Dcms\Dealers;

/**
 *
 * @author web <web@groupdc.be>
 */
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class DealersServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $this->loadViewsFrom(realpath(__DIR__ . '/resources/views'), 'dcms');
        $this->setupRoutes($this->app->router);
        // this  for conig
        $this->publishes([
            __DIR__ . '/config/dcms_sidebar.php' => config_path('Dcms/Dealers/dcms_sidebar.php'),
            __DIR__. '/public/assets' => public_path('packages/Dcms/Dealers'),
        ]);

        if (!is_null(config('dcms.dealers'))) {
            $this->app['config']['dcms_sidebar'] = array_merge((array) $this->app["config"]["dcms_sidebar"], config('dcms.dealers.dcms_sidebar'));
        }
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function setupRoutes(Router $router)
    {
        $router->group(['namespace' => 'Dcms\Dealers\Http\Controllers'], function ($router) {
            require __DIR__ . '/Http/routes.php';
        });
    }

    public function register()
    {
    }
}
