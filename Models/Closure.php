<?php

namespace Dcms\Dealers\Models;

use Dcms\Core\Models\EloquentDefaults;

class Closure extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = "dealers_closure";


    public function dealer()
    {
        return $this->hasOne('\Dcms\Dealers\Models\Dealer', 'id', 'dealer_id');
    }
}
