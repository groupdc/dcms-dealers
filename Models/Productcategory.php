<?php

namespace Dcms\Dealers\Models;

use DB;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Productcategory extends Model
{
    use NodeTrait;

    protected $connection = 'project';
    protected $table  = "products_categories_language";
    protected $fillable = array('language_id', 'title');
}
