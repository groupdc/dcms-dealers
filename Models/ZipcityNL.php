<?php

namespace Dcms\Dealers\Models;

use Dcms\Core\Models\EloquentDefaults;

class ZipcityNL extends EloquentDefaults
{
    protected $connection = 'admin';

    protected $table = "zip_nl";
}
