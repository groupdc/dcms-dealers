<?php

namespace Dcms\Dealers\Models;

use Dcms\Core\Models\EloquentDefaults;

class Zipcity extends EloquentDefaults
{
    protected $connection = 'admin';

    protected $table = "zipcode-be";
}
