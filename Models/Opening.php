<?php

namespace Dcms\Dealers\Models;

use Dcms\Core\Models\EloquentDefaults;

class Opening extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "dealers_opening";
    protected $fillable  = ['dealer_id', 'opening_day'];

    public function dealer()
    {
        return $this->hasOne('\Dcms\Dealers\Models\Dealer', 'id', 'dealer_id');
    }
}
