<?php

namespace Dcms\Dealers\Models;

use Dcms\Core\Models\EloquentDefaults;

class Dealer extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = "dealers";

    public function pages()
    {
        // BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
        return $this->belongsToMany('Dcms\Pages\Models\Pageslanguage', 'dealers_to_pages', 'dealer_id', 'page_id')->withTimestamps();
    }

    public function category()
    {
        return $this->belongsToMany('Dcms\Dealers\Models\Productcategory', 'dealers_to_productcategory', 'dealer_id', 'category_id')->withTimestamps();
    }

    public function openings()
    {
        return $this->hasMany(Opening::class);
    }
}
