<?php
return [
    "Dealers" => [
        "icon"  => "fa-map-marker-alt",
        "links" => [
            ["route" => "/admin/dealers", "label" => "Dealers", "permission" => 'dealers'],
            ["route" => "/admin/dealers/map", "label" => "Map", "permission" => 'dealers']
        ],
    ],
];
