<?php

Route::group(['middleware' => ['web']], function () {
    Route::group(array("prefix" => "admin", "as"=>"admin."), function () {
        Route::group(['middleware' => 'auth:dcms'], function () {

            //DEALERS
            Route::group(array("prefix" => "dealers", "as"=>"dealers."), function () {
                Route::get("{id}/copy", array("as"=>"copy", "uses" => "DealerController@copy"));
                Route::get("api/zipcity", array("as"=>"api.zipcity", "uses" => "DealerController@getZipCityJson"));
                Route::any("api/table", array( "as" => "api.table", "uses" => "DealerController@getDatatable"));
                Route::any("api/closurerow", array("as"=>"api.closurerow", "uses" => "DealerController@getClosureRow"));
                Route::any("map", array( "as" => "map", "uses" => "DealerController@map"));
                Route::any("fillnullgps", array( "as" => "map", "uses" => "DealerController@fillNullGps"));
                Route::get("api/table/categories/{dealerid?}", array("as"=>"api.table.categories", "uses"=>"DealerController@getCategoriesDatatable"));
            });
            Route::resource("dealers", "DealerController");
        });
    });
});
