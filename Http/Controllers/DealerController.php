<?php

namespace Dcms\Dealers\Http\Controllers;

use DB;
use Auth;
use Form;
use View;
use Schema;
use Session;
use DateTime;
use Redirect;
use Validator;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Dcms\Dealers\Models\Dealer;
use Dcms\Dealers\Models\Closure;
use Dcms\Dealers\Models\Opening;
use Dcms\Dealers\Models\Zipcity;
use Dcms\Dealers\Models\ZipcityNL;
use Dcms\Core\Http\Controllers\BaseController;

class DealerController extends BaseController
{
    public $countries = [];
    public $extendFormTemplate = "";
    public $ColumNames = [];
    public $ColumnNamesDefaults = []; // e.g. checkboxes left blank will result in NULL database value, if this is not what you want, you can set e.g. array('checkbox_name'=>'0');

    public function __construct()
    {
        $this->middleware('permission:dealers-browse')->only('index');
        $this->middleware('permission:dealers-add')->only(['create', 'store']);
        $this->middleware('permission:dealers-edit')->only(['edit', 'update']);
        $this->middleware('permission:dealers-delete')->only('destroy');

        $this->extendformTemplate = null;
        $this->ColumNames = array_merge($this->ColumNames, ['dealer' => 'dealer', 'address' => 'address', 'zip' => 'zip', 'city' => 'city', 'country_id' => 'country_id', 'phone' => 'phone', 'email' => 'email', 'contactemail' => 'contactemail', 'attention'=>'attention', 'website' => 'website']);
        $this->ColumNames = array_merge($this->ColumNames, ['premium'=>'premium','propremium'=>'propremium',  'procenter'=>'procenter', 'tuincenter'=>'tuincenter', 'webshop' => 'webshop', 'code'=>'code', 'code_NL'=>'code_NL', 'naturapy'=>'naturapy', 'naturapypickup'=>'naturapypickup' ,'contactlanguage' => 'contactlanguage' ]);
        $this->ColumnNamesDefaults = array_merge($this->ColumnNamesDefaults, ['country_id' => '1']);
        $this->ColumnNamesDefaults = array_merge($this->ColumnNamesDefaults, [ 'premium'=>'0','propremium'=>'0',  'procenter'=>'0', 'tuincenter'=>'0', 'webshop'=>'0',  'naturapy'=>'0', 'naturapypickup'=>'0']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $filtercolumns = ['dealer' => 'dealer', 'city' => 'city', 'country' => 'country (ISO 3166-2)', 'procenter' => 'pro', 'sector' => 'sector'];
        return View::make('dcms::dealers/index')->with('filtercolumns', $filtercolumns);
    }

    public function map(Request $request)
    {
        $mapMarkers = "";

        if ($request->server("REQUEST_METHOD") == "POST") {
            $SQLDealers = Dealer::where('country_id', 'LIKE', '%');
            if ($request->has('country_id') && $request->get('country_id') > 0) {
                $SQLDealers = Dealer::where('country_id', 'LIKE', $request->get('country_id'));
            }

            $SQLDealers = $SQLDealers->where('id', '>', '0');
            if (strlen($request->get('search')) > 0) {
                $SQLDealers = $SQLDealers->where(function ($query) use ($request) {
                    $query->where('dealer', 'LIKE', $request->get('search'));
                    $query->orWhere('address', 'LIKE', $request->get('search'));
                    $query->orWhere('city', 'LIKE', $request->get('search'));
                });
            }
            if ($request->get('tuincentrum') == 1) {
                $SQLDealers = $SQLDealers->where('tuincenter', '=', '1');
            }
            if ($request->get('procenter') == 1) {
                $SQLDealers = $SQLDealers->where('procenter', '=', '1');
            }
            if ($request->get('premium') == 1) {
                $SQLDealers = $SQLDealers->where('premium', '=', '1');
            }
            if ($request->get('propremium') == 1) {
                $SQLDealers = $SQLDealers->where('propremium', '=', '1');
            }
            if ($request->get('naturapy') == 1) {
                $SQLDealers = $SQLDealers->where('naturapy', '=', '1');
            }
            if ($request->get('naturapypickup') == 1) {
                $SQLDealers = $SQLDealers->where('naturapypickup', '=', '1');
            }
                $SQLDealers = $SQLDealers->whereNotNull('longitude');
                $SQLDealers = $SQLDealers->whereNotNull('latitude');
            $Dealers = $SQLDealers->get();

            if ($Dealers->count() > 0) {
                foreach ($Dealers as $Dealer) {
                    $thisDealer = "";

                    $detaillink = "http://www.dcm-info.be/nl/hobby/verkooppunten/";

                    if ($Dealer->country_id == '2' && $Dealer->procenter == '1') {
                        $detaillink = "http://www.dcm-info.nl/pro/verkooppunten/";
                    } elseif ($Dealer->country_id == '2' && $Dealer->procenter == '0') {
                        $detaillink = "http://www.dcm-info.nl/hobby/verkooppunten/";
                    } elseif ($Dealer->country_id == '3') {
                        $detaillink = "http://www.dcm-info.fr/hobby/points-de-vente/";
                    }

                    $thisDealer = $thisDealer . "<h3><a href='" . $detaillink . $Dealer->id . "/" . Str::slug($Dealer->dealer) . "/' target='_parent'> " . $Dealer->dealer . "</a></h3>";
                    $http = "http";

                    if (intval($Dealer->premium) == 1) {
                        $thisDealer = $thisDealer . "<img src='" . $http . "://www.dcm-info.com/images/dealer/nl/premium.png' alt='Premium' class='premium' />";
                    }
                    if (intval($Dealer->webshop) == "1") {
                        $thisDealer = $thisDealer . "<a href='" . $Dealer->website . "'><img src='" . $http . "://www.dcm-info.com/images/dealer/nl/webshop.png' alt='webshop' class='webshop' /></a>";
                    }
                    $thisDealer .= "<p>";

                    if (strlen(trim($Dealer->address)) > 0) {
                        $thisDealer .= $Dealer->address . "<br />";
                    }

                    if (strlen(trim($Dealer->zip)) > 0) {
                        $thisDealer .= $Dealer->zip . " " . $Dealer->city . "<br />";
                    }

                    if (strlen(trim($Dealer->phone)) > 0) {
                        $thisDealer .= "<i class='far fa-phone'></i> " . $Dealer->phone . "<br />";
                    }
                    //if(strlen(trim($Dealer->fax))>0) 		$thisDealer .= "<i class='far fa-print'></i> " . $Dealer->fax . "<br />" ;
                    if (strlen(trim($Dealer->email)) > 0) {
                        $thisDealer .= "<i class='far fa-envelope'></i> <a href='mailto:" . $Dealer->email . "'>" . $Dealer->email . "</a><br />";
                    }

                    if (strlen($Dealer->website) > 0) {
                        if (strpos($Dealer->website, "http://") >= 0 || strpos($Dealer->website, "https://") >= 0) {
                            $website = $Dealer->website;
                        } else {
                            $website = "http://" . $Dealer->website;
                        }
                        $thisDealer = $thisDealer . "<i class='far fa-laptop'></i> <a href='" . $website . "' target='_blank'> " . str_replace("http://", "", $website) . "</a><br />";
                    } //'end if website check
                    /*'==============================================================================================================================================================*/

                    $thisDealer .= "</p>";
                    $thisDealer .= '<form method="POST" action="/admin/dealers/' . $Dealer->id . '" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="' . csrf_token() . '"> <input name="_method" type="hidden" value="DELETE"><a class="btn btn-xs btn-default" href="/admin/dealers/' . $Dealer->id . '/edit"><i class="far fa-pencil"></i></a><button class="btn btn-xs btn-default" type="submit" value="Delete this article" onclick="if(!confirm(\'Are you sure to delete this item?\')){return false;};"><i class="far fa-trash-alt"></i></button></form>';
                    $icon = "gmap_pin1.png";
                    $type = "default";

                    if ($Dealer->premium == 1 && $Dealer->country_id == 1) {
                        $icon = "gmap_pin3.png";
                        $type = "premium";
                    }


                    if ($Dealer->premium == 1 && $Dealer->country_id <> 1) {
                        $icon = "gmap_pin5.png";
                        $type = "extended";
                    }

                    $mapMarkers .=  ",{
                        position: new google.maps.LatLng(" . $Dealer->latitude . ", " . $Dealer->longitude . "),
                        title: '".str_replace("'", "", $Dealer->dealer)."',
                        info: '".str_replace("'", '"', $thisDealer)."',
                        zindex: ". $Dealer->id.",
                        type: '".$type."'
                    }";
                }
                $mapMarkers = substr($mapMarkers, 1);
            }
        }
        // load the view
        return View::make('dcms::dealers/map')->withInput($request->all())->with('mapMarkers', $mapMarkers);
    }

    public function getZipCityJson(Request $request)
    {
        $zipcity = trim($request->get("zipcity"));
        $country_id = trim($request->get("country_id"));

        if ($country_id == "1") {
            return Zipcity::select('postcode as zip', 'gemeente as city', (DB::connection('admin')->table('admin')->raw('CONCAT(postcode, " ", gemeente) AS label')))->where('postcode', 'LIKE', $zipcity . '%')->orwhere('gemeente', 'LIKE', '%' . $zipcity . '%')->get()->toJson();
        } else {
            return ZipcityNL::select('postcode as zip', 'gemeente as city', (DB::connection('admin')->table('admin')->raw('CONCAT(postcode, " ", gemeente) AS label')))->where('postcode', 'LIKE', $zipcity . '%')->orwhere('gemeente', 'LIKE', '%' . $zipcity . '%')->get()->toJson();
        }
    }

    public function getDatatable()
    {
        $query = DB::connection('project')
            ->table('dealers')->select("dealers.id", "dealers.dealer", "dealers.code", "dealers.address", "dealers.zip", "dealers.city", (DB::connection("project")->raw("concat(\"<img src='/packages/Dcms/Core/images/flag-\", lcase(countries.country),\".png' > \") as country")), 'longitude', 'latitude')
            ->leftJoin("countries", "countries.id", "=", "dealers.country_id");

        if (Session::has('dealerfilter.colname')) {
            $filter = Session::get('dealerfilter');
            foreach ($filter['colname'] as $i => $colname) {
                if ($colname != '0' && $colname != 'sector') {
                    $query->where($colname, 'LIKE', $filter['colvalue'][$i]);
                } elseif ($colname == 'sector') {
                    $query->whereRaw('dealers.id IN (SELECT id FROM vwdealers_to_pages WHERE sector LIKE ? )', [$filter['colvalue'][$i]]);
                }
            }
        } elseif (intval(session('overrule_default_by_country_id')) > 0) {
            $query->where('country_id', intval(session('overrule_default_by_country_id')));
        }

        return DataTables::queryBuilder($query)
            ->addColumn('edit', function ($model) {
                $edit = '<form method="POST" action="/admin/dealers/' . $model->id . '" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="' . csrf_token() . '"> <input name="_method" type="hidden" value="DELETE">';
                if (Auth::user()->can('dealers-edit')) {
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/dealers/' . $model->id . '/edit"><i class="far fa-pencil-alt"></i></a>';
                }
                if (Auth::user()->can('dealers-delete')) {
                    $edit .= '<button class="btn btn-xs btn-default" type="submit" value="Delete this article" onclick="if(!confirm(\'Are you sure to delete this item ? \')){return false;};"><i class="far fa-trash-alt"></i></button>';
                }
                $edit .= '</form>';

                return $edit;
            })
            ->rawColumns(['country', 'edit'])
            ->make(true);
    }

    public function getCategoriesDatatable($dealer_id = 0)
    {
        $queryA = DB::connection('project')
        ->table('products_categories_language as x')
        ->select(
            (
                DB::connection("project")->raw('
                case when (select count(*) from dealers_to_productcategory where dealers_to_productcategory.dealer_id = "'.$dealer_id.'" and dealers_to_productcategory.category_id = x.id) > 0 then 1 else 0 end as checked,
                x.id,
                dealerlabel as title,
                languages.country')
            )
        )
        ->whereNotNull('dealerlabel')
        ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
        ->groupBy('language_id', 'dealerlabel')
        ->orderBy('checked', 'DESC');

        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from dealers_to_productcategory where dealers_to_productcategory.dealer_id = "'.$dealer_id.'" and dealers_to_productcategory.category_id = x.id) > 0 then 1 else 0 end = 1');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('x.language_id', session('overrule_default_by_language_id'));
        }

        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
                ->addColumn('radio', function ($model) {
                    return '<input type="checkbox" name="category_id[]" value="'.$model->id.'" '.($model->checked == 1 ? 'checked="checked"' : '').' id="chkbox_'.$model->id.'" > ';
                })
                ->addColumn('language', function ($model) {
                    return '<img src="/packages/Dcms/Core/images/flag-'.strtolower($model->country).'.svg" style="width:16px; height: auto;" alt="">';
                })
                ->rawColumns(['radio','language'])
                ->make(true);
    }

    public function getSelectedPages($dealerid = null)
    {
        return DB::connection("project")->select('	SELECT dealer_id, page_id
													FROM dealers_to_pages
													WHERE dealer_id =  ?', [$dealerid]);
    }

    public function fillNullGps()
    {
        $dealers = Dealer::where('country_id', '=', 1)->whereNull('latitude')->take(200)->get();
        foreach ($dealers as $Dealer) {
            $latlon["lon"] = null;
            $latlon["lat"] = null;
            $latlon = $this->get_GPSCoordinates($Dealer->zip, $Dealer->city, $Dealer->address, 'belgium');
            $Dealer->longitude = $latlon["lon"];
            $Dealer->latitude = $latlon["lat"];
            $Dealer->save();
        }

        $dealers = Dealer::where('country_id', '=', 2)->whereNull('latitude')->take(200)->get();
        foreach ($dealers as $Dealer) {
            $latlon["lon"] = null;
            $latlon["lat"] = null;
            $latlon = $this->get_GPSCoordinates($Dealer->zip, $Dealer->city, $Dealer->address, 'netherlands');
            $Dealer->longitude = $latlon["lon"];
            $Dealer->latitude = $latlon["lat"];
            $Dealer->save();
        }
        return "saved";
    }

    /* returns an array holding Lat Lon coordinates of the given location
         this is used to fetch the gpscoordinates when looking for  dealers
        ************************************************************/
    public function get_GPSCoordinates($zip, $city, $address, $country)
    {
        $geozipcode = trim(strip_tags($zip));
        $geozipcode = str_replace(" ", "", $geozipcode);
        $geocity = trim(strip_tags($city));
        $geoaddress = trim(strip_tags($address));
        $geoaddress = str_replace(" ", "+", $geoaddress);
        $geoquery = "";
        $geoquery .= "+" . $country;
        if (strlen($geozipcode) > 0) {
            $geoquery .= "+" . $geozipcode;
        }
        if (strlen($geocity) > 0) {
            $geoquery .= "+" . $geocity;
        }
        if (strlen($geoaddress) > 0) {
            $geoquery .= "+" . $geoaddress;
        }
        $geoquery = substr($geoquery, 1); // remove the first +
        $googleurl = "https://maps.googleapis.com/maps/api/geocode/json?key=" . env('API_GOOGLE_MAPS_KEY') . "&address=" . str_replace(' ', '+', $geoquery);

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $googledata = json_decode(file_get_contents($googleurl, false, stream_context_create($arrContextOptions)));
        $aCoordinates["lat"] = 0;
        $aCoordinates["lon"] = 0;
        if ($googledata->status == "OK") {
            $aCoordinates["lat"] = $googledata->results[0]->geometry->location->lat;
            $aCoordinates["lon"] = $googledata->results[0]->geometry->location->lng;
        }
        return $aCoordinates;
    }

    public function getCountries()
    {
        $oCountries = DB::connection("project")->table("countries")->select("id", "country_name")->get();
        if (!is_null($oCountries)) {
            foreach ($oCountries as $c) {
                $this->countries[$c->id] = $c->country_name;
            }
        }
    }

    public function setPageOptionValues($objselected_pages)
    {
        $pageOptionValuesSelected = [];
        if (count($objselected_pages) > 0) {
            foreach ($objselected_pages as $obj) {
                $pageOptionValuesSelected[$obj->page_id] = $obj->page_id;
            }
        }
        return $pageOptionValuesSelected;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->getCountries();
        return View::make('dcms::dealers/form')
            ->with("countries", $this->countries)
            ->with('extendformTemplate', ['template' => $this->extendformTemplate/*,'model' => $this->getExtendedModel()*/])
            ->with('pageOptionValuesSelected', [])
            ->with('openings', collect(new Opening()));
    }

    public function saveDealer(Request $request, $dealerid = null)
    {
        if (!is_null($dealerid) && intval($dealerid) > 0) {
            $Dealer = Dealer::find($dealerid);
        }
        if (!isset($Dealer) || is_null($Dealer)) {
            $Dealer = new Dealer();
        }

        $regex = "";
        if ($request->has('country_id') && $request->get('country_id') == 1) {
            $regex = '/([0-9]{4})/';
        } elseif ($request->has('country_id') && $request->get('country_id') == 9) {
            $regex = '/([0-9]{4})/';
        } elseif ($request->has('country_id') && $request->get('country_id') == 2) {
            $regex = '/([0-9]{4})\s?([a-zA-Z]{2})/';
        } else {
            $regex = '/([0-9]{5})/';
        }

        preg_match_all($regex, trim($request->get('zip')), $matches, PREG_SET_ORDER, 0);
        $matches = array_replace_recursive([0=>[0=>1000,1=>1000,2,1000]], (array)$matches);

        $zip = null;
        if ($request->get('country_id') == 2) {
            $zip = $matches[0][1].' '.strtoupper($matches[0][2]);
        } elseif ($request->get('country_id') == 1) {
            $zip = $matches[0][1];
        }

        foreach ($this->ColumNames as $column => $inputname) {
            if (!$request->has($inputname) && array_key_exists($inputname, $this->ColumnNamesDefaults)) {
                $Dealer->$column = trim($this->ColumnNamesDefaults[$inputname]);
            } else {
                if ($column == "zip" && !is_null($zip)) {
                    $Dealer->$column = $zip;
                } else {
                    $Dealer->$column = trim($request->get($inputname));
                }
            }
        }

        $this->getCountries();
        $latlon = $this->get_GPSCoordinates($Dealer->zip, $Dealer->city, $Dealer->address, $this->countries[$Dealer->country_id]);

        if (!empty($latlon['lon'])) {
            $Dealer->longitude = $latlon["lon"];
        } else {
            $Dealer->longitude = $request->get('longitude');
        }

        if (!empty($latlon['lat'])) {
            $Dealer->latitude = $latlon["lat"];
        } else {
            $Dealer->latitude = $request->get('latitude');
        }
        if ($Dealer->website == "http://") {
            $Dealer->website = "";
        }
        $Dealer->save();
        $this->StoreDealerPage($request, $Dealer);
        $this->StoreDealerClosure($request, $Dealer);
        $this->StoreDealerOpening($request, $Dealer);
        $this->StoreDealerCategory($request, $Dealer);
        return $Dealer;
    }

    public function StoreDealerCategory($request, $Dealer)
    {
        $Dealer->category()->detach();

        if ($request->has('category_id')) {
            foreach ($request->get('category_id') as $category_id) {
                $Dealer->category()->attach($category_id);
            }
        }
    }

    public function StoreDealerPage($request, $Dealer)
    {
        //----------------------------------------
        // Detach the $Dealer  from all pages
        //----------------------------------------
        $Dealer->pages()->detach();

        //----------------------------------------
        // link the article to the selected page
        // we will take the $Detail->id since
        // this directly holds the language_id
        // otherwise we'd be storing it twice
        //----------------------------------------
        if ($request->has('page_id') && count($request->get('page_id')) > 0) {
            foreach ($request->get('page_id') as $language_id => $aPages) {
                foreach ($aPages as $page_id) {
                    $Dealer->pages()->attach($page_id);
                }
            }
        }//end - if(count($input["page_id"][$language_id])>0)
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'code'   => 'required|unique:dealers,code',
            'dealer' => 'required',
            'opening_hour_1.*' => 'date_format:H:i|nullable',
            'opening_hour_2.*' => 'date_format:H:i|nullable',
            'opening_hour_3.*' => 'date_format:H:i|nullable',
            'opening_hour_4.*' => 'date_format:H:i|nullable',
        ];

        $regex = "";
        if ($request->has('country_id') && $request->get('country_id') == 1) {
            $regex = '/([0-9]{4})/';
            $rules = array_merge($rules, ['zip'=>['required','min:4','max:4','regex:'.$regex]]);
            $rules = array_merge($rules, ['shipment_zip'=>['nullable','required_if:shipment_address,1','min:4','max:4','regex:'.$regex]]);
        } elseif ($request->has('country_id') && $request->get('country_id') == 9) {
            $regex = '/([0-9]{4})/';
            $rules = array_merge($rules, ['zip'=>['required','min:4','max:4','regex:'.$regex]]);
            $rules = array_merge($rules, ['shipment_zip'=>['nullable','required_if:shipment_address,1','min:4','max:4','regex:'.$regex]]);
        } elseif ($request->has('country_id') && $request->get('country_id') == 2) {
            $regex = '/([0-9]{4})\s?([a-zA-Z]{2})/';
            $rules = array_merge($rules, ['zip'=>['required','min:4','max:7','regex:'.$regex]]);
            $rules = array_merge($rules, ['shipment_zip'=>['nullable','required_if:shipment_address,1','min:4','max:7','regex:'.$regex]]);
        } else {
            $regex = '/([0-9]{5})/';
        }

        $validator = Validator::make($request->all(), $rules);
        // process the validator
        if ($validator->fails()) {
            return Redirect::to('admin/dealers/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $this->saveDealer($request);
            Session::flash('message', 'Successfully created dealer!');
            return Redirect::to('admin/dealers');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dealer = Dealer::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dealer = Dealer::find($id);
        $openings = $dealer->openings;
        $this->getCountries();
        $mClosures = DB::connection("project")->select('SELECT id, dealer_id, closure_startdate, closure_enddate FROM dealers_closure WHERE dealer_id = ? ORDER BY 1 ', [$id]);
        $rowClosure = $this->getClosureRow($mClosures, false);
        $objselected_pages = $this->getSelectedPages($id);

        // show the edit form and pass the nerd
        return View::make('dcms::dealers/form')
            ->with('dealer', $dealer)
            ->with("countries", $this->countries)
            ->with('rowClosure', $rowClosure)
            ->with('extendformTemplate', ['template' => $this->extendformTemplate/*,'model' => $this->getExtendedModel()*/])
            ->with('pageOptionValuesSelected', $this->setPageOptionValues($objselected_pages))
            ->with('openings', $openings);
    }

    /**
     * copy the model
     *
     * @param  int $id
     *
     * @return Response
     */
    public function copy($id)
    {
        $new = Dealer::find($id)->replicate();
        $new->save();
        return Redirect::to('admin/dealers');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'code'   => 'required|unique:dealers,code,'.$id,
            'dealer'           => 'required',
            'opening_hour_1.*' => 'date_format:H:i|nullable',
            'opening_hour_2.*' => 'date_format:H:i|nullable',
            'opening_hour_3.*' => 'date_format:H:i|nullable',
            'opening_hour_4.*' => 'date_format:H:i|nullable',
        ];

        $regex = "";
        if ($request->has('country_id') && $request->get('country_id') == 1) {
            $regex = '/([0-9]{4})/';
            $rules = array_merge($rules, ['zip'=>['required','min:4','max:4','regex:'.$regex]]);
            $rules = array_merge($rules, ['shipment_zip'=>['nullable','required_if:shipment_address,1','min:4','max:4','regex:'.$regex]]);
        } elseif ($request->has('country_id') && $request->get('country_id') == 9) {
            $regex = '/([0-9]{4})/';
            $rules = array_merge($rules, ['zip'=>['required','min:4','max:4','regex:'.$regex]]);
            $rules = array_merge($rules, ['shipment_zip'=>['nullable','required_if:shipment_address,1','min:4','max:4','regex:'.$regex]]);
        } elseif ($request->has('country_id') && $request->get('country_id') == 2) {
            $regex = '/([0-9]{4})\s?([a-zA-Z]{2})/';
            $rules = array_merge($rules, ['zip'=>['required','min:4','max:7','regex:'.$regex]]);
            $rules = array_merge($rules, ['shipment_zip'=>['nullable','required_if:shipment_address,1','min:4','max:7','regex:'.$regex]]);
        } else {
            $regex = '/([0-9]{5})/';
            $rules = array_merge($rules, ['zip'=>['required','min:4','max:7','regex:'.$regex]]);
            $rules = array_merge($rules, ['shipment_zip'=>['nullable','required_if:shipment_address,1','min:4','max:7','regex:'.$regex]]);
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('admin/dealers/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            $this->saveDealer($request, $id);
            Session::flash('message', 'Successfully updated dealer!');
            return Redirect::to('admin/dealers');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $article = Dealer::find($id);
        $article->delete();
        Session::flash('message', 'Successfully deleted the dealer!');
        return Redirect::to('admin/dealers');
    }

    /**
     * $mDefaults contains an array of Price-Models
     *
     * @return the row to inject prices
     */
    public function getClosureRow($mDefaults = [], $forceEmpty = true)
    {
        $aRowString = [];
        $rowstring = "";
        $encloseBody = true;

        if ($forceEmpty === true && empty($mDefaults) === true) {
            $encloseBody = false;
            $mDefaults[] = (object) [];
        }

        foreach ($mDefaults as $Closure) {
            //------------------------------------------------------------------------
            // 							TEMPLATE FOR THE PRICE ROW
            // 		the {INDEX} tag will be replaced in the form.blade, and this script to the attachment database id - or some text to identify its new
            //------------------------------------------------------------------------

            $rowstring .= ' <tr>
								<td>
									' . Form::text('closure_startdate[{INDEX}]', isset($Closure->closure_startdate) ? (is_null($Closure->closure_startdate) ? null : DateTime::createFromFormat('Y-m-d H:i:s', $Closure->closure_startdate)->format('d/m/Y')) : '', ['id' => 'Startdate{INDEX}', 'class' => 'form-control Startdate', 'size' => '16']) . '
								</td>
								</td>
								<td>
									' . Form::text('closure_enddate[{INDEX}]', isset($Closure->closure_enddate) ? (is_null($Closure->closure_enddate) ? null : DateTime::createFromFormat('Y-m-d H:i:s', $Closure->closure_enddate)->format('d/m/Y')) : '', ['id' => 'Enddate{INDEX}', 'class' => 'form-control Enddate', 'size' => '16']) . '
								</td>
								<td><a class="btn btn-default pull-right delete-table-row" href=""><i class="far fa-trash-alt"></i></a></td>
							</tr>';

            if (isset($Closure->id) && intval($Closure->id) > 0) {
                $rowstring = str_replace("{INDEX}", $Closure->id, $rowstring);
            }
        }

        if ($encloseBody === true) {
            $rowstring = '<tbody>' . $rowstring . '</tbody>';
        }
        return $rowstring;
    }//end of funciton getClosureRow

    protected function StoreDealerClosure(Request $request, Dealer $Dealer)
    {
        $donotdeleteids = [];
        //---------------------------------------------
        // PRODUCT Attachemnts (Availability per language_id)
        //---------------------------------------------
        if ($request->has("closure_startdate") && count($request->get("closure_startdate")) > 0) {
            foreach ($request->get('closure_startdate') as $closure_id => $closuretime) {
                $mClosure = null;
                $mClosure = Closure::find($closure_id);  //we make an update when we get an PIM-id(products_data.id) from the form
                if (is_null($mClosure) === true) {  // if we couln't find a Model for the given PIM-id we need to create/add a new one.
                    $mClosure = new Closure();
                }

                $mClosure->dealer_id = $Dealer->id;
                $mClosure->closure_startdate =($request->has('closure_startdate.'.$closure_id) ? DateTime::createFromFormat('d/m/Y', $request->get('closure_startdate')[$closure_id])->format('Y-m-d') : null);
                $mClosure->closure_enddate = ($request->has('closure_enddate.'.$closure_id) ? DateTime::createFromFormat('d/m/Y', $request->get('closure_enddate')[$closure_id])->format('Y-m-d') : null);
                $mClosure->save();
                $donotdeleteids[$mClosure->id] = $mClosure->id;
            }
        }

        $Closure = Closure::where('dealer_id', '=', $Dealer->id);
        if (count($donotdeleteids) > 0) {
            $Closure->whereNotIn('id', $donotdeleteids);
        }
        $Closure->delete();
    }

    protected function StoreDealerOpening(Request $request, Dealer $dealer)
    {
        for ($i = 1; $i <= 7; $i++) {
            $opening = Opening::firstOrNew(['dealer_id' => $dealer->id, 'opening_day' => $i]);
            if ($request->has('open_' . $i)) {
                $opening->opening_hour_1 = $request->get('opening_hour_1')[$i];
                $opening->opening_hour_2 = $request->get('opening_hour_2')[$i];
                $opening->opening_hour_3 = $request->get('opening_hour_3')[$i];
                $opening->opening_hour_4 = $request->get('opening_hour_4')[$i];
                $opening->save();
            } else {
                $opening->delete();
            }
        }
    }
}
